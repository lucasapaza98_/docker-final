<?php
require_once "class/connection.php";
$obj = new conectar();
$conexion = $obj->conexion();

if (isset($_SESSION['email'])) {
    $email_sesion = $_SESSION['email'];
    $rol_session = $_SESSION['name_role'];

    $sql  = "SELECT u.id_user,u.name,u.last_name,r.name_rol,u.email FROM users AS u 
                    INNER JOIN roles AS r ON u.id_rol = r.id_rol WHERE u.email = '$email_sesion' AND r.name_rol = '$rol_session'";

    $result = mysqli_query($conexion, $sql);

    if ($ver = mysqli_fetch_row($result) > 0) {

        $rol_session2 = $ver[3];
        $id_user_session = $ver[0];
    }

}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sistemas ventas - Login</title>

    <link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.css">
    <script src="libraries/jquery-3.7.1.min.js"></script>
    <script src="js/functions.js"></script>


</head>

<body>
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="panel panel-info">
                    <div class="p-3 mb-2 bg-info text-dark" style="text-align:center;"> Sistemas de Ventas y de Almacen</div>
                    <div class="panel panel-body">
                        <p></p>

                        <form id="frmLogin">
                            <label for="email">Email</label>
                            <input type="email" class="form-control input-sm" name="email" id="email" max="20">
                            <!--    <label for="rol">rol</label>
                            <input type="email" class="form-control input-sm" name="rol" id="rol" max="20">
                             -->
                            <label for="pass">Contraseña</label>
                            <input type="password" class="form-control input-sm" name="pass" id="pass">
                            <p></p>
                            <br>
                            <span class="btn btn-info btn-m" id="loginSesion">Iniciar sesion</span>

                            <?php if (!$validar):
                            ?>
                                <a href="register.php" class="btn btn-primary btn-m">Registrarse</a>
                            <?php endif;
                            ?>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">

            </div>


        </div>
    </div>


</body>

</html>

<script type="text/javascript">
    $(document).ready(function() {
        $('#loginSesion').click(function() {

            vacios = validarFormVoid('frmLogin');

            if (vacios > 0) {
                alert("Debe llenar todos los campos");
                return false;
            }


            datos = $('#frmLogin').serialize();

            $.ajax({
                type: "POST",
                data: datos,
                url: "process/regLogin/login.php",
                success: function(r) {
                    if (r == 1) {
                        window.location = "views/inicio.php";
                        console.log("RESULTADO " + r);
                        console.log("DATOS " + datos);
                        // console.log(type);
                        //console.log(url)

                        //alert("exito");
                    } else {

                        console.log("RESULTADO " + r);
                        console.log("DATOS " + datos);
                        //console.log(type);
                        alert("No se pudo acceder :(");

                    }

                }
            });
        });

    });
</script>

<?php
/*require_once "class/connection.php";
$obj = new conectar();
$conexion = $obj->conexion();

$sql = "SELECT * FROM users WHERE email='admin@gmail.com'";
$result = mysqli_query($conexion, $sql);
$validar = 0;

if (mysqli_num_rows($result) > 0) {
    $validar = 1;
}


/*var_dump($_SESSION['email']);
print_r("<br>");
var_dump($_SESSION['id_user']);

var_dump($validar);
var_dump($sql);
*/
?>