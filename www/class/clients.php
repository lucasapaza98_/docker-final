<?php

class Client
{

    public function agg_client($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $id_user = $_SESSION['id_user'];

        $sql = "INSERT INTO customers (id_user,
										name,
										last_name,
										direction,
										email,
										phone,
										rfc)
							values ('$id_user',
									'$datos[0]',
									'$datos[1]',
									'$datos[2]',
									'$datos[3]',
									'$datos[4]',
									'$datos[5]')";
        return mysqli_query($conexion, $sql);
    }

    //VALIDAR LOS DATOS DUPLICADOS DE LOS CLIENTES 
	public function buscaRepetidoCli($id_user, $direccion,$email,$phone,$rfc, $conexion)
	{

		$c = new conectar();
		$conexion = $c->conexion();

		$sql = "SELECT * FROM customers WHERE id_user='$id_user' AND direction='$direccion' 
                                                                 AND email='$email' 
                                                                 AND phone = '$phone'
                                                                 AND rfc = '$rfc'";

		$result = mysqli_query($conexion, $sql);

		if (mysqli_num_rows($result) > 0) {
			return 1;
		} else {
			return 0;
		}
	}

    public function obtenDatosCliente($idcliente)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT id_cli, 
							name,
							last_name,
							direction,
							email,
							phone,
							rfc 
				from customers where id_cli='$idcliente'";
        $result = mysqli_query($conexion, $sql);
        $ver = mysqli_fetch_row($result);

        $datos = array(
            'id_cli' => $ver[0],    
            'name' => $ver[1],
            'last_name' => $ver[2],
            'direction' => $ver[3],
            'email' => $ver[4],
            'phone' => $ver[5],
            'rfc' => $ver[6]
        );
        return $datos;
    }

    public function update_client($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();
        $sql = "UPDATE customers SET name='$datos[1]',
										last_name='$datos[2]',
										direction='$datos[3]',
										email='$datos[4]',
										phone='$datos[5]',
										rfc='$datos[6]' 
								where id_cli='$datos[0]'";
        return mysqli_query($conexion, $sql);
    }

    public function deleteClient($idcliente)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "DELETE from customers where id_cli='$idcliente'";

        return mysqli_query($conexion, $sql);
    }
}
