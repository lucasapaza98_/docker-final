<?php

//use LDAP\Result;

class Articles
{
    public function aggImagen($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $date = date('Y-m-d');

        $sql = "INSERT INTO images(id_cat,name_img,route,upload_date) 
        VALUES('$datos[0]','$datos[1]','$datos[2]','$date')";

        $result = mysqli_query($conexion, $sql);

        return mysqli_insert_id($conexion);
    }

    public function insert_articles($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $date = date('Y-m-d');

        $sql = "INSERT INTO articles(id_cat,id_img,id_user,name_prod,description,amount,price,capture_date)
             VALUES('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$datos[5]','$datos[6]','$date')";


        return mysqli_query($conexion, $sql);
    }

    public function buscaRepetidoArt($nombre, $descripcion, $conexion){

		$c = new conectar();
		$conexion = $c->conexion();

        $sql = "SELECT * FROM articles WHERE name_prod = '$nombre' AND description = '$descripcion'";
       
        $result = mysqli_query($conexion, $sql);

        if (mysqli_num_rows($result) > 0) {
			return 1;
		} else {
			return 0;
		}

    }




    public function obten_data_articles($id_articles)
    {

        $c = new conectar();
        $conexion = $c->conexion();


        $sql = "SELECT id_prod, id_cat,name_prod,description,amount,price 
                FROM articles WHERE id_prod='$id_articles'";

        $result = mysqli_query($conexion, $sql);
        
        $ver = mysqli_fetch_row($result);

        $datos = array(
            "id_prod" => $ver[0],
            "id_cat" => $ver[1],
            "name_prod" => $ver[2],
            "description" => $ver[3],
            "amount" => $ver[4],
            "price" => $ver[5]
        );

        return $datos;
    }

    public function update_articulos($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "UPDATE articles SET id_cat='$datos[1]', 
                                    name_prod='$datos[2]',
                                    description='$datos[3]',
                                    amount='$datos[4]',
                                    price='$datos[5]'
                               WHERE id_prod='$datos[0]'";

        return mysqli_query($conexion, $sql);
    }

    public function delete_articles($id_articles)
    {

        $c = new conectar();
        $conexion = $c->conexion();

        $id_images = self::obtenIdImg($id_articles);
        $sql = "DELETE * FROM articles WHERE id_prod='$id_articles'";
        
        $result = mysqli_query($conexion, $sql);

        if ($result) {
            $ruta = self::obtenRutaImagen($id_images);
            $sql = "DELETE * FROM images where id_img='$id_images'";

            $result = mysqli_query($conexion, $sql);

            if ($result) {
                if (unlink($ruta)) {
                    return 1;
                }
            }
        }
    }

    public function obtenIdImg($idProducto)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT id_img 
                from articles 
                where id_prod='$idProducto'";
        $result = mysqli_query($conexion, $sql);

        return mysqli_fetch_row($result)[0];
    }

    public function obtenRutaImagen($idImg)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT route 
                from images 
                where id_img='$idImg'";

        $result = mysqli_query($conexion, $sql);

        return mysqli_fetch_row($result)[0];
    }
}
