<?php

class Sales
{
    public function obtenDataProducto($id_producto)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT  art.name_prod,
                        art.description,
                        art.amount,
                        img.route,
                        art.price 
                FROM articles AS art
                INNER JOIN images AS img
                ON art.id_img=img.id_img
                WHERE id_prod='$id_producto'";

        $result = mysqli_query($conexion, $sql);

        $ver = mysqli_fetch_row($result);

        $d = explode('/', $ver[3]);
        $img = $d[1] . '/' . $d[2] . '/' . $d[3];

        $datos = array(
            'name_prod' => $ver[0],
            'description' => $ver[1],
            'amount' => $ver[2],
            'route' => $img,
            'price' => $ver[4]

        );
        return $datos;
    }

    public function crearVenta()
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $date = date('Y-m-d');
        $idventa = self::creaFolio();
        $datos = $_SESSION['tablaCompraTemp'];
        $iduser = $_SESSION['id_user'];
        $r = 0;

        for ($i = 0; $i < count($datos); $i++) {
            $d = explode('||', $datos[$i]);



            /*$articulo = $id_producto . "||" .
            $nombre_producto . "||" .
            $descripcionV . "||" .
            $precioV . "||" .
            $ncliente . "||" .
            $id_cliente;
            */

            $sql = "INSERT INTO sales(id_vent,id_cli,id_prod,id_user,price,shopping_date)
                            VALUES ('$idventa','$d[5]','$d[0]','$iduser','$d[3]','$date')";

            $r = $r + $result = mysqli_query($conexion, $sql);
        }
        return $r;
    }

    public function creaFolio()
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT id_vent from sales group by id_vent desc";

        $resul = mysqli_query($conexion, $sql);
        $id = mysqli_fetch_row($resul)[0];

        if ($id == "" or $id == null or $id == 0) {
            return 1;
        } else {
            return $id + 1;
        }
    }

    ///***************************************
    public function nombreClientegg($idCliente)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT last_name,name 
			from customers 
			where id_cli='$idCliente'";
        $result = mysqli_query($conexion, $sql);

        $ver = mysqli_fetch_row($result);

        return $ver[0] . " " . $ver[1];
    }

    public function obtenerTotal($idventa)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT price 
				from sales
				where id_vent='$idventa'";
        $result = mysqli_query($conexion, $sql);

        $total = 0;

        while ($ver = mysqli_fetch_row($result)) {
            $total = $total + $ver[0];
        }

        return $total;
    }
}
