<?php

class user
{
    public function registerUser($datos)
    {

        $c = new conectar();
        $conexion = $c->conexion();

        $date = date('Y-m-d');

        $sql = "INSERT INTO users(name,last_name,id_rol,email,pass,capture_date)
                VALUES('$datos[0]','$datos[1]','$datos[2]','$datos[3]','$datos[4]','$date')";
        
        //$_SESSION['name_role'] = self::obtenDatosUsers($_SESSION['id_user'])['name_rol'];

        return mysqli_query($conexion, $sql);
    }

    //VALIDAR A LOS USUARIOS REPETIDOS

    public function buscarRepetido($email,$pass,$conexion){

        $c = new conectar();
        $conexion = $c->conexion();
        
        $sql = "SELECT * FROM users WHERE email='$email' AND pass='$pass'";
        
        $result = mysqli_query($conexion, $sql);
    
        if (mysqli_num_rows($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    
    }
    

    public function loginUser($datos)
    {

        $c = new conectar();
        $conexion = $c->conexion();
        $password = sha1($datos[1]);

        $_SESSION['email'] = $datos[0];
        $_SESSION['id_user'] = self::sesionID($datos);

        $_SESSION['rol'] = self::obtenDatosUsers($_SESSION['id_user'])['id_rol'];  
        $_SESSION['name_role'] = self::obtenDatosUsers($_SESSION['id_user'])['name_rol'];
        $_SESSION['name_user'] = self::obtenDatosUsers($_SESSION['id_user'])['name'];
        $_SESSION['last_name'] = self::obtenDatosUsers($_SESSION['id_user'])['last_name'];
        



        $sql = "SELECT * FROM users WHERE email='$datos[0]' AND pass='$password'";

        //var_dump($sql); 

        //exit;

        $result = mysqli_query($conexion, $sql);

        if (mysqli_num_rows($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function sesionID($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $password = sha1($datos[1]);

        $sql = "SELECT id_user FROM users WHERE email='$datos[0]' AND pass='$password'";

        $result = mysqli_query($conexion, $sql);

        return mysqli_fetch_row($result)[0];
    }

    public function obtenDatosUsers($id_user)
    {

        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "SELECT id_user,name,last_name,id_rol,email FROM users WHERE id_user='$id_user'";
        $result = mysqli_query($conexion, $sql);

        $ver = mysqli_fetch_row($result);

        $datos = array(
            'id_user' => $ver[0],
            'name' => $ver[1],
            'last_name' => $ver[2],
            'id_rol' => $ver[3],
            'email' => $ver[4]
        );

        return $datos;
    }

    public function ActualizaUsers($datos)
    {
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "UPDATE users SET name='$datos[1]',
                                 last_name='$datos[2]',
                                 id_rol='$datos[3]',
                                 email='$datos[4]' 
                                WHERE id_user='$datos[0]'";
                                
        return mysqli_query($conexion,$sql);
    }

    public function eliminaUsers($id_user){
        $c = new conectar();
        $conexion = $c->conexion();

        $sql = "DELETE  FROM users WHERE id_user='$id_user'";

        return mysqli_query($conexion, $sql);

    }
}
