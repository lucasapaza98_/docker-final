CREATE TABLE users (
    id_user int(11),
    name varchar(50),
    last_name varchar(50),
    id_rol int(11),
    email varchar(100),
    pass varchar(100),
    capture_date date,
);

CREATE TABLE roles(
    id_rol int(11),
    name_rol varchar(50)
);

CREATE TABLE categories(
    id_cat int(11),
    id_user int(11) NOT NULL,
    cat_name varchar(250),
    capture_date date
);
CREATE TABLE images(
    id_img int(11),
    id_cat int(11) not null,
    name_img varchar(50) default null,
    route varchar(50) default null,
    upload_date date default null
);
CREATE TABLE articles (
    id_prod int(11),
    id_cat int(11) not null,
    id_img int (11) not null,
    id_user int(11) not null,
    name_prod varchar(50),
    description varchar(50),
    amount int(11),
    price float,
    capture_date date
);

CREATE TABLE customers(
    id_cli int,
    id_user int not null,
    name varchar(50),
    last_name varchar(50),
    direction varchar(200),
    email varchar(200),
    phone varchar(200),
    rfc varchar(200)
);
CREATE TABLE sales(
    id_vent int not null,
    id_cli int,
    id_prod int,
    id_user int,
    price float,
    shopping_date date
);


ALTER TABLE users
ADD PRIMARY KEY (id_user);
ALTER TABLE users
MODIFY id_user int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE roles
ADD PRIMARY KEY (id_rol);
ALTER TABLE roles
MODIFY id_rol int(11) NOT NULL AUTO_INCREMENT;




ALTER TABLE categories
ADD PRIMARY KEY (id_cat);
ALTER TABLE categories
MODIFY id_cat int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE images
ADD PRIMARY KEY (id_img);
ALTER TABLE images
MODIFY id_img int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE articles
ADD PRIMARY KEY (id_prod);
ALTER TABLE articles
MODIFY id_prod int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE customers
ADD PRIMARY KEY (id_cli);
ALTER TABLE customers
MODIFY id_cli int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE images
ADD PRIMARY KEY (id_img)
ALTER TABLE images
MODIFY id_img int(11) NOT NULL AUTO_INCREMENT;