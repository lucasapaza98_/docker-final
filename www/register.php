

<?php
require_once "class/connection.php";
$obj = new conectar();
$conexion = $obj->conexion();

if (isset($_SESSION['email'])) {
    $email_sesion = $_SESSION['email'];
    $rol_session = $_SESSION['name_role'];

    $sql  = "SELECT u.id_user,u.name,u.last_name,r.name_rol,u.email FROM users AS u 
                    INNER JOIN roles AS r ON u.id_rol = r.id_rol WHERE u.email = '$email_sesion' AND r.name_rol = '$rol_session'";

    $result = mysqli_query($conexion, $sql);

    if ($ver = mysqli_fetch_row($result) > 0) {

        $rol_session = $ver[3];
        $id_user_session = $ver[0];
        $name_session = $ver[1];
        $last_name_session = $ver[2];
    }

}

var_dump($rol_session);
var_dump($id_user_session);
var_dump($name_session);




?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistemas ventas - Register</title>
    <link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.css">
    <script src="libraries/jquery-3.7.1.min.js"></script>
    <script src="js/functions.js"></script>

</head>

<body>
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="panel panel-danger">

                    <div class="p-3 mb-2 bg-danger text-white" style="text-align:center;">Registrar Administrador</div>
                    <div class="panel panel-body">
                        <form id="frmRegister">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control input-sm" name="nombre" id="nombre" max="20">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control input-sm" name="apellido" id="apellido" max="20">
                            <label for="rol">Rol</label>
                            <input type="text" class="form-control input-sm" name="rol" id="rol" max="20">
                            <label for="email">Email</label>
                            <input type="email" class="form-control input-sm" name="email" id="email">
                            <label for="pass">Contraseña</label>
                            <input type="password" class="form-control input-sm" name="pass" id="pass">
                            <br>
                            <span class="btn btn-primary btn-m" id="register">Registrar</span>
                            <a href="index.php" class="btn btn-info btn-m">Volver</a>

                        </form>
                    </div>

                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</body>

</html>


<script>
    $(document).ready(function() {

        //script para evento click y ajax 
        $('#register').click(function() {

            vacios = validarFormVoid('frmRegister');

            if (vacios > 0) {
                alert("Debe llenar todos los campos");
                return false;
            }
            datos = $('#frmRegister').serialize();
           


            /*datos = "name=" + $('#nombre').val() +
                    "&last_name=" + $('#apellido').val() +
                    "&email=" + $('#email').val() +
                    "&pass=" + $('#pass').val();
                    */
            $.ajax({
                type: "POST",
                data: datos,
                url: "process/regLogin/registerUser.php",
                success: function(r) {
                    if (r == 2) {
                        alert("Usuario duplicado ingrese otro");

                    } else if (r == 1) {
                        //console.log(r);
                        //console.log(datos);
                        alert("Exit");
                    } else {
                        console.log("DATOS register.php: " . datos);
                        console.log(r);
                        
                        alert("Error");
                    }

                }
            });
        });



    });
</script>