<?php require_once "dependence.php" ?>

<!DOCTYPE html>
<html>

<head>
  <title></title>
</head>

<body>
  <nav class="navbar navbar-expand-lg" style="background-color: #e3f2fd;">
  <div class="container-fluid">
   
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" aria-current="page" href="inicio.php">Inicio</a>
        </li>

        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" href="categories.php">Categorias</a>
        </li>
      
        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="articles.php">Articulos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="client.php">Clientes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="sales.php">Vender Articulos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="rol.php">Roles</a>
        </li>


      </ul>

      <a href="#" style="color:green; font-weight: bold; ">NOMBRE USUARIO: <?php echo $_SESSION['name_user'] ;?> <?php  echo $_SESSION['last_name'];?> <span class="caret"></span></a>
      <div class="d-flex">



      <?php
            if ($_SESSION['email'] == "admin@gmail.com"):
            ?>
              <a href="users.php"> <button class="btn btn-outline-success" type="submit">Usuarios</button></a>

            <?php
            endif;
            ?>
      
     
       <a href="../process/salir.php"> <button class="btn btn-outline-success" type="submit">Salir</button></a>
    </div>
  </div>
</nav>
</body>

</html>
