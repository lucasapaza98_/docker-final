<?php
require_once "../../class/connection.php";
require_once "../../class/sales.php";

$c = new conectar();
$conexion = $c->conexion();

$obj = new Sales();

$sql = "SELECT id_vent,shopping_date,id_cli FROM sales GROUP BY id_vent";
$result = mysqli_query($conexion, $sql);



?>

<h4>Ventas y Reportes</h4>
<div class="row">
	<div class="col-sm-1"></div>
	<div class="col-sm-10">
		<div class="table_responsive">
			<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
				<caption><label>Ventas</label></caption>
				<tr style="font-weight: bold;">
					<td>Folio</td>
					<td>Fecha</td>
					<td>Cliente</td>
					<td>Total de compra</td>
					<td>Ticket</td>
					<td>Reporte</td>
				</tr>
				<?php while ($ver = mysqli_fetch_row($result)): ?>

					<tr>
						<td><?php echo $ver[0] ?></td>
						<td><?php echo $ver[1] ?></td>
						<td>
							<?php
							if ($obj->nombreClientegg($ver[2]) == " ") {

								echo "SIN_CLI";
							} else {
								echo $obj->nombreClientegg($ver[2]);
							}
							?>
						</td>
						<td>
							<?php
							echo "$" . $obj->obtenerTotal($ver[0]);
							?>

						</td>
						<td>
							<a href="../process/sales/createTicketPDF.php?idventa=<?php echo $ver[0];?>" class="btn btn-danger btn-sm">Tick</a>

						</td>
						<td>
							<a href="../process/sales/createReportPDF.php?idventa=<?php echo $ver[0];?>" class="btn btn-danger btn-sm">Report</a>

						</td>
					</tr>
				<?php endwhile; ?>

			</table>
		</div>
	</div>

	<div class="col-sm-1"></div>
</div>