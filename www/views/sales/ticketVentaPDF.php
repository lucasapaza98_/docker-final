<?php
require_once "../../class/connection.php";
require_once "../../class/sales.php";

$objv = new Sales();


$c = new conectar();
$conexion = $c->conexion();

$idventa = $_GET['idventa'];

$sql = "SELECT ve.id_vent,
                ve.shopping_date,
                ve.id_cli,
                art.name_prod,
                art.amount,
                art.price,
                art.description
                from sales  as ve 
                inner join articles as art
                on ve.id_prod=art.id_prod
                and ve.id_vent='$idventa'";

$result = mysqli_query($conexion, $sql);

$ver = mysqli_fetch_row($result);

$folio = $ver[0];
$fecha = $ver[1];
$idcliente = $ver[2];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistemas Ventas - TICKET</title>
    <link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.css">


    <style type="text/css">
        @page {
            margin-top: 0.3em;
            margin-left: 0.6em;
        }

        body {
            font-size: xx-small;
        }
    </style>


</head>

<body>
    <!--   <img src="../../img/sistventa.jpg" alt="" width="200" height="200">-->
    <p>Sistemas Ventas</p>
    <p>
        Fecha: <?php echo $fecha ?>
    </p>
    <p>
        Folio:<?php echo $folio ?>
    </p>
    <p>
        Cliente:<?php echo $objv->nombreClientegg($idventa); ?>
    </p>
    <br>

    <table style="border-collapse: collapse;" class="table" border="1">
        <tr>
            <td>Nombre</td>
            <td>Precio</td>
        </tr>
        <?php
        $sql = "SELECT ve.id_vent,
        ve.shopping_date,
        ve.id_cli,
        art.name_prod,
        art.amount,
        art.price,
        art.description
        from sales  as ve 
        inner join articles as art
        on ve.id_prod=art.id_prod
        and ve.id_vent='$idventa'";

        $result = mysqli_query($conexion, $sql);
        $total = 0;

        while ($mostrar = mysqli_fetch_row($result)) {

        ?>
            <tr>
                <td><?php echo $mostrar[3]; ?></td>
                <td><?php echo $mostrar[5]; ?></td>
            </tr>
        <?php
            $total = $total + $mostrar[5];
        }
        ?>
        <tr>
            <td>Total = <?php echo "$." . $total; ?> </td>
        </tr>
    </table>


</body>

</html>