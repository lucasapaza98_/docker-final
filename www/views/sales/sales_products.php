<?php
require_once "../../class/connection.php";

$c = new conectar();
$conexion = $c->conexion();


?>

<h4>Vender productos</h4>
<div class="row">
	<div class="col-sm-4">
		<form id="frmVentasProductos">

			<label>Seleccionar Clientes</label>
			<select class="form-control input-sm" id="clienteVenta" name="clienteVenta">

				<option value="A">Seleccionar</option>
				<option value="0">Sin clientes</option>

				<?php $sql = "SELECT id_cli,name,last_name FROM customers";
				$result = mysqli_query($conexion, $sql);

				while ($cliente = mysqli_fetch_row($result)):
				?>
					<option value="<?php echo $cliente[0] ?>"><?php echo $cliente[2] . " " . $cliente[1] ?></option>
				<?php endwhile; ?>

			</select>

			<label>Producto</label>
			<select class="form-control input-sm" id="productoVenta" name="productoVenta">
				<option value="A">Seleccionar</option>

				<?php
				$sql = "SELECT id_prod,name_prod FROM articles";
				$result = mysqli_query($conexion, $sql);

				while ($producto = mysqli_fetch_row($result)):

				?>
					<option value="<?php echo $producto[0]; ?>"><?php echo $producto[1]; ?></option>
				<?php
				endwhile;
				?>
			</select>

			<label>Descripcion</label>
			<textarea name="descripcionV" id="descripcionV" class="form-control input-sm" disabled></textarea>

			<label>Cantidad</label>
			<input type="number" class="form-control input-sm" id="cantV" name="cantV" disabled>

			<label>Precio</label>
			<input type="number" class="form-control input-sm" id="precioV" name="precioV" disabled>

			<span class="btn btn-primary" id="btnAgregaVenta">Agregar</span>
			<span class="btn btn-danger" id="btnVaciarVenta">Vaciar</span>

		</form>

	</div>
	<div class="col-sm-3">
		<div id="imgProducto"></div>
	</div>

	<div class="col-sm 4">
		<div id="tablaVentaTempLoad"></div>
	</div>
</div>

<script>
	$(document).ready(function() {

		$("#tablaVentaTempLoad").load("sales/tablaVentaTemp.php");


		$("#productoVenta").change(function() {
			//alert("hola");
			$.ajax({
				type: "POST",
				data: "id_producto=" + $("#productoVenta").val(),
				url: "../process/sales/fullFormProductos.php",
				success: function(r) {

					console.log(r);

					dato = jQuery.parseJSON(r);

					$("#descripcionV").val(dato['description']);
					$("#cantV").val(dato['amount']);
					$("#precioV").val(dato['price']);

					$("#imgProducto").prepend('<img class="img-thumbnail" id="imgp" alt="" src="' + dato['route'] + '">')



				}
			});

		});

		//script para evento click y ajax 
		$('#btnAgregaVenta').click(function() {
			vacios = validarFormVoid('frmVentasProductos');

			if (vacios > 0) {
				alertify.alert("Debes llenar todos los campos!!");
				return false;
			}

			datos = $('#frmVentasProductos').serialize();
			console.log(datos);

			$.ajax({
				type: "POST",
				data: datos,
				url: "../process/sales/agregaProductoTemp.php",
				success: function(r) {
					console.log(r);
					$("#tablaVentaTempLoad").load("sales/tablaVentaTemp.php");

				}
			});
		});

		//script para evento click y ajax 
		$('#btnVaciarVenta').click(function() {

			datos = $('#frmVentasProductos').serialize();
			console.log(datos);
			$.ajax({
				url: "../process/sales/vaciarTemp.php",
				success: function(r) {
					console.log(r);
					$("#tablaVentaTempLoad").load("sales/tablaVentaTemp.php");
				}
			});
		});

	});
</script>

<script>
	function quitarP(index) {
		$.ajax({
			type: "POST",
			data: 'ind=' + index,
			url: "../process/sales/quitProducto.php",
			success: function(r) {
				console.log(r);
				$("#tablaVentaTempLoad").load("sales/tablaVentaTemp.php");
				
				console.log(r);
				alertify.success("Se quito el producto");

			}
		});
	}

	function crearSales() {
		$.ajax({
			url: "../process/sales/createSales.php",
			success: function(r) {
				if (r > 0) {
					$("#tablaVentaTempLoad").load("sales/tablaVentaTemp.php");
					$("#frmVentasProductos")[0].reset();
					console.log(r);
					alertify.alert("Venta creada con exito, consultar la informacion de esta ventas hechas");

				} else if (r == 0) {
					console.log(r);
					alertify.alert("No hay ventas");


				} else{
					console.log(r);
					alertify.error("No se pudo crear ventas");
				}

			}
		});

	}
</script>



<script type="text/javascript">
	$(document).ready(function() {
		$("#clienteVenta").select2();
		$("#productoVenta").select2();
		console.log($("#clienteVenta"));
		console.log($("#productoVenta"));

	});
</script>