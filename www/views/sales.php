<?php
session_start();
if(isset($_SESSION['email'])){

    //echo $_SESSION['user']; 
   var_dump($_SESSION['email']);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 

    <title>Sistemas Ventas - Ventas</title>
    
    <?php require_once "main.php";?>

</head>
<body>

<div class="container">
    <h1>Ventas de productos</h1>
    <div class="row">
        <div class="col-sm-12">
            <span class="btn btn-default" id="ventasProductosBtn">Ventas productos</span>
            <span class="btn btn-default" id="ventasHechasBtn">Ventas hechas</span>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="ventasProductos"></div>
            <div id="ventasHechas"></div>
        </div>
    </div>
 </div>
</div>
    
</body>
</html>

<script>
    $(document).ready(function(){

        $("#ventasProductosBtn").click(function(){

            esconderSeccionVenta()
            $("#ventasProductos").load('sales/sales_products.php');
            $("#ventasProductos").show();

        }); 

        $("#ventasHechasBtn").click(function(){

            esconderSeccionVenta()
            $("#ventasHechas").load('sales/sales_report.php');
            $("#ventasHechas").show();

        });

        function esconderSeccionVenta(){

            $("#ventasProductos").hide();
            $("#ventasHechas").hide();


        }

    });
</script>

<?php

}       
else{
    header("location:../index.php");

}

?>