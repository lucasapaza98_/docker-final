<?php
session_start();
$email_users_session = $_SESSION['email'];


if (isset($_SESSION['email']) and $_SESSION['email'] == "$email_users_session") {

    //echo $_SESSION['user']; 
    var_dump($_SESSION['email']);


?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sistemas Ventas - Usuario</title>
        <?php require_once "main.php"; ?>
        <?php
        require_once "../class/connection.php";
        $c = new conectar();
        $conexion = $c->conexion();
        $sql = "SELECT id_rol,name_rol
           from roles";
        $result = mysqli_query($conexion, $sql);
        ?>
    </head>

    <body>
        <br><br><br><br><br>
        <div class="container">
            <h1>Administrar usuarios</h1>
            <div class="row">
                <div class="col-sm-4">
                    <form id="frmRegister">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control input-sm" name="nombre" id="nombre">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control input-sm" name="apellido" id="apellido">
                        <label for="rolSelect">Rol</label>
                        <select class="form-control input-sm" id="rolSelect" name="rolSelect">
                            <option value="A">Selecciona rol</option>
                            <?php while ($ver = mysqli_fetch_row($result)): ?>
                                <option value="<?php echo $ver[0] ?>"><?php echo $ver[1] ?></option>
                            <?php endwhile; ?>
                        </select>

                        <label for="user_register">Email</label>
                        <input type="email" class="form-control input-sm" name="email" id="email">


                        <label for="pass_register">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="pass" id="pass">
                        <br>
                        <span class="btn btn-primary btn-m" id="register">Registrar</span>
                    </form>
                </div>
                <div class="col-sm-7">
                    <div id="tablaUserLoad"></div>

                </div>
            </div>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="updateUsersModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Actualizar Usuarios</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="frmRegisterU">
                            <input type="text" id="id_user" name="id_user" hidden>
                            <label for="nombreU">Nombre</label>
                            <input type="text" class="form-control input-sm" name="nombreU" id="nombreU">
                            <label for="apellidoU">Apellido</label>
                            <input type="text" class="form-control input-sm" name="apellidoU" id="apellidoU">

                            <label for="rolSelectU">Rol</label>
                            <select class="form-control input-sm" id="rolSelectU" name="rolSelectU">
                                <option value="A">Selecciona rol</option>
                                <?php
                                $sql = "SELECT id_rol,name_rol
                                 from roles";
                                $result = mysqli_query($conexion, $sql);
                                ?>
                                <?php while ($ver = mysqli_fetch_row($result)): ?>
                                    <option value="<?php echo $ver[0] ?>"><?php echo $ver[1] ?></option>
                                <?php endwhile; ?>
                            </select>


                            <label for="user_registerU">Email</label>
                            <input type="email" class="form-control input-sm" name="user_registerU" id="user_registerU">
                            <br>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-bs-dismiss="modal" id="btnUpdateUsers">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>


    </body>

    </html>

    <script>
        function aggDatoUsers(id_user) {

            $.ajax({
                type: "POST",
                data: "id_user=" + id_user,
                url: "../process/users/obtenDatoUsuario.php",
                success: function(r) {

                    dato = jQuery.parseJSON(r);
                    console.log(dato);
                    $('#id_user').val(dato['id_user']);
                    $('#nombreU').val(dato['name']);
                    $('#apellidoU').val(dato['last_name']);
                    $('#rolSelectU').val(dato['id_rol']);
                    $('#user_registerU').val(dato['email']);


                }
            });


        }

        function delete_user(id_user) {
            alertify.confirm('Desea eliminar el usuario', function() {
                $.ajax({
                    type: "POST",
                    data: "id_user=" + id_user,
                    url: "../process/users/deleteUsers.php",
                    success: function(r) {
                        if (r == 1) {
                            console.log(r);
                            $("#tablaUserLoad").load('users/table_users.php');
                            alertify.success("Eliminado con exito");

                        } else {
                            console.log(r);

                            alertify.error("Error al eliminar");

                        }

                    }
                });
            }, function() {
                alertify.error('Cancelado')
            });

        }
    </script>

    <script>
        $(document).ready(function() {

            //script para evento click y ajax 
            $('#btnUpdateUsers').click(function() {

                datos = $('#frmRegisterU').serialize();
                console.log(datos);
                $.ajax({
                    type: "POST",
                    data: datos,
                    url: "../process/users/update_users.php",
                    success: function(r) {
                        console.log(r);
                        if (r == 1) {
                            $('#frmRegister')[0].reset();
                            $("#tablaUserLoad").load('users/table_users.php');
                            alertify.success("Actualizado con exit");
                        } else {
                            alertify.error("Error al actualizar");
                        }

                    }
                });
            });

        });
    </script>


    <script>
        $(document).ready(function() {

            $('#tablaUserLoad').load("users/table_users.php");


            //script para evento click y ajax 
            $('#register').click(function() {

                vacios = validarFormVoid('frmRegister');

                if (vacios > 0) {
                    alert("Debe llenar todos los campos");
                    return false;
                }

                datos = $('#frmRegister').serialize();


                /* datos = "name=" + $('#nombre').val() +
                     "&last_name=" + $('#apellido').val() +
                     "&email=" + $('#email').val() +
                     "&pass=" + $('#pass').val();
                     */

                $.ajax({
                    type: "POST",
                    data: datos,
                    url: "../process/regLogin/registerUser.php",
                    success: function(r) {

                        if (r == 2) {
                            alertify.alert("Usuario duplicado ingrese otro");
                            console.log(r);


                        } else if (r == 1) {
                            $('#frmRegister')[0].reset();
                            $("#tablaUserLoad").load('users/table_users.php');
                            console.log(r);


                            alertify.success("Agregado con exito");
                            // console.log(r);
                        } else {
                            console.log(r);
                            alertify.error("Error Al registrar");
                        }

                    }
                });
            });



        });
    </script>

<?php

} else {
    header("location:../index.php");
}

?>