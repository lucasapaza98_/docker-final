<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 


<link rel="stylesheet" href="../libraries/alertifyjs/css/alertify.css">
<link rel="stylesheet" href="../libraries/alertifyjs/css/themes/default.css">
<link rel="stylesheet" href="../libraries/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="../libraries/select2/css/select2.css">
<link rel="stylesheet" href="../css/main.css">

<script src="../libraries/jquery-3.7.1.min.js"></script>
<script src="../libraries/alertifyjs/alertify.js"></script>
<script src="../libraries/bootstrap/js/bootstrap.js"></script>
<script src="../libraries/select2/js/select2.js"></script>
<script src="../js/functions.js"></script>