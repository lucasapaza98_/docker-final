<?php
require_once "../../class/connection.php";

$obj = new conectar();
$conexion = $obj->conexion();

$sql = "SELECT id_cli, 
				name,
				last_name,
				direction,
				email,
				phone,
				rfc 
		from customers";
$result = mysqli_query($conexion, $sql);

//var_dump($sql);
?>

<div class="table-responsive">
	<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
		<caption><label>Clientes</label></caption>
		<tr style="font-weight: bold;">
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Direccion</td>
			<td>Email</td>
			<td>Telefono</td>
			<td>RFC</td>
			<td>Editar</td>
			<td>Eliminar</td>
		</tr>

		<?php while ($ver = mysqli_fetch_row($result)): ?>

			<tr>
				<td><?php echo $ver[1]; ?></td>
				<td><?php echo $ver[2]; ?></td>
				<td><?php echo $ver[3]; ?></td>
				<td><?php echo $ver[4]; ?></td>
				<td><?php echo $ver[5]; ?></td>
				<td><?php echo $ver[6]; ?></td>
				<td>
					<span class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#abremodalClientesUpdate" onclick="agg_data_client('<?php echo $ver[0]; ?>')">
						<span class="glyphicon glyphicon-pencil">Act</span>
					</span>
				</td>
				<td>
					<span class="btn btn-danger btn-sm" onclick="delete_cliente('<?php echo $ver[0]; ?>')">
						<span class="glyphicon glyphicon-remove">Elim</span>
					</span>
				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>