<?php
session_start();
if (isset($_SESSION['email'])) {
	
	var_dump($_SESSION['email']);

	


?>


	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistemas Ventas - Roles</title>
		
		<?php require_once "main.php"; ?>
	</head>

	<body>
		<div class="container">
			<h1>Roles</h1>
			<div class="row">
				<div class="col-sm-4">
					<form id="frmRoles">
						<label>Rol</label>
						<input type="text" class="form-control input-sm" name="rolV" id="rolV">
						<p></p>
						<span class="btn btn-primary" id="btnAgregaRoles">Agregar</span>
					</form>
				</div>
				<div class="col-sm-6">
					<div id="tablaRolesLoad"></div>
				</div>
			</div>
		</div>



		<!-- Modal -->
		<div class="modal fade" id="update_rol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Actualizar Rol</h4>
					</div>
					<div class="modal-body">
						<form id="frmRolesU">
							<input type="text" name="id_rol" id="id_rol" hidden>
							<label>Rol</label>
							<input type="text" name="rolU" id="rolU" class="form-control input-sm">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btn_update_rol" class="btn btn-warning" data-dismiss="modal">Actualizar</button>
					</div>
				</div>
			</div>
		</div>

		



	</body>

	</html>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#tablaRolesLoad').load("roles/table_roles.php");

			$('#btnAgregaRoles').click(function() {

				vacios = validarFormVoid('frmRoles');

				if (vacios > 0) {
					alertify.alert("Debes llenar todos los campos!!");
					return false;
				}

				datos = $('#frmRoles').serialize();
				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/roles/agg_roles.php",
					success: function(r) {
						if (r == 1) {
							//esta linea nos permite limpiar el formulario al insertar un registro
							$('#frmRoles')[0].reset();
							$('#tablaRolesLoad').load("roles/table_roles.php");
							alertify.success("Roles agregada con exito");

							console.log(r);

						} else {

							console.log(r);

							alertify.error("No se pudo agregar el rol");
						}
					}
				});
			});
		});
	</script>

	<script>
		$(document).ready(function() {
			$('#btn_update_rol').click(function() {
				datos=$('#frmRolesU').serialize();
				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/roles/update_roles.php",
					success:function(r) {

						if (r == 1) {
							$('#tablaRolesLoad').load("roles/table_roles.php");
							alertify.success("Actualizado con exito");

						} else {
							alertify.error("No se pudo actualizar");
						}

					}
				});
			});

		});
	</script>

	<script type="text/javascript">
		function agg_datos_rol(id_rol,roles) {
			$('#id_rol').val(id_rol);
			$('#rolU').val(roles);


			//console.log(id_rol, roles);
			//console.log(id_rol);
			//console.log(roles);


		}

	</script>
<?php
} else {
	header("location:../index.php");
}

?>