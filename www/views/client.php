<?php
session_start();
if (isset($_SESSION['email'])) {

	var_dump($_SESSION['email']);

?>


	<!DOCTYPE html>
	<html>

	<head>
		<title>Sistemas de ventas - Clientes</title>
		<?php require_once "main.php"; ?>
	</head>

	<body>
		<div class="container">
			<h1>Clientes</h1>
			<div class="row">
				<div class="col-sm-3">
					<form id="frmClientes">
						<label>Nombre</label>
						<input type="text" class="form-control input-sm" id="nombre" name="nombre">
						<label>Apellido</label>
						<input type="text" class="form-control input-sm" id="apellidos" name="apellidos">
						<label>Direccion</label>
						<input type="text" class="form-control input-sm" id="direccion" name="direccion">
						<label>Email</label>
						<input type="email" class="form-control input-sm" id="email" name="email">
						<label>Telefono</label>
						<input type="number" class="form-control input-sm" id="telefono" name="telefono">
						<label>RFC</label>
						<input type="number" class="form-control input-sm" id="rfc" name="rfc">
						<p></p>
						<span class="btn btn-primary" id="btnAgregarCliente">Agregar</span>
					</form>
				</div>
				<div class="col-sm-9">
					<div id="tablaClientesLoad"></div>
				</div>
			</div>
		</div>

		<!-- Button trigger modal -->

		<!-- Modal -->
		<div class="modal fade" id="abremodalClientesUpdate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Actualizar Clientes</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form id="frmClientesU">
							<input type="text" id="id_clienteU" name="id_clienteU" hidden>
							<label>Nombre</label>
							<input type="text" class="form-control input-sm" id="nombreU" name="nombreU">
							<label>Apellido</label>
							<input type="text" class="form-control input-sm" id="apellidosU" name="apellidosU">
							<label>Direccion</label>
							<input type="text" class="form-control input-sm" id="direccionU" name="direccionU">
							<label>Email</label>
							<input type="email" class="form-control input-sm" id="emailU" name="emailU">
							<label>Telefono</label>
							<input type="number" class="form-control input-sm" id="telefonoU" name="telefonoU">
							<label>RFC</label>
							<input type="number" class="form-control input-sm" id="rfcU" name="rfcU">
							<p></p>
							<!-- <span class="btn btn-primary" id="btnAgregaClient">Agregar</span> -->
						</form>

					</div>
					<div class="modal-footer">
						<button type="button" id="btnAgregaClientU" class="btn btn-warning" data-bs-dismiss="modal">Actualizar</button>

					</div>
				</div>
			</div>
		</div>


	</body>

	</html>

	<script type="text/javascript">
		function agg_data_client(idcliente) {

			$.ajax({
				type: "POST",
				data: "idcliente=" + idcliente,
				url: "../process/client/obten_data_client.php",
				success: function(r) {
					console.log(r);
					dato = jQuery.parseJSON(r);
					$('#idclienteU').val(dato['id_cli']);
					$('#nombreU').val(dato['name']);
					$('#apellidosU').val(dato['last_name']);
					$('#direccionU').val(dato['direction']);
					$('#emailU').val(dato['email']);
					$('#telefonoU').val(dato['phone']);
					$('#rfcU').val(dato['rfc']);

				}
			});
		}

		function delete_cliente(idcliente) {
			alertify.confirm('¿Desea eliminar este cliente?', function() {
				$.ajax({
					type: "POST",
					data: "idcliente=" + idcliente,
					url: "../process/client/delete_client.php",
					success: function(r) {
						if (r == 1) {
							$('#tablaClientesLoad').load("client/table_client.php");
							alertify.success("Eliminado con exito!!");
						} else {
							alertify.error("No se pudo eliminar :(");
						}
					}
				});
			}, function() {
				alertify.error('Cancelo !')
			});
		}
	</script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#tablaClientesLoad').load("client/table_client.php");

			$('#btnAgregarCliente').click(function() {

				vacios = validarFormVoid('frmClientes');

				if (vacios > 0) {
					alertify.alert("Debes llenar todos los campos!!");
					return false;
				}


				datos = $('#frmClientes').serialize();
				//console.log(datos);

				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/client/agg_client.php",
					success: function(r) {

						if(r == 2){
							alertify.alert("Cliente duplicado ingrese otro");
							console.log("DUPLICADO".r);

						} else if (r == 1) {
							$('#frmClientes')[0].reset();
							$('#tablaClientesLoad').load("client/table_client.php");
							alertify.success("Cliente agregado con exito");
						} else {
							alertify.error("No se pudo agregar cliente");
						}
					}
				});
			});
		});
	</script>


	<script>
		$(document).ready(function() {
			$('#btnAgregaClientU').click(function() {
				datos = $('#frmClientesU').serialize();
				console.log(datos);

				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/client/update_client.php",
					success: function(r) {

						if (r == 1) {
							$('#frmClientes')[0].reset();
							$('#tablaClientesLoad').load("client/table_client.php");
							alertify.success("Cliente actualizado con exito");
						} else {
							alertify.error("No se pudo actualizar cliente");
						}
					}
				});
			})
		})
	</script>


<?php
} else {
	header("location:../index.php");
}
?>