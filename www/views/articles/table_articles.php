<?php

use LDAP\Result;

require_once "../../class/connection.php";
$c = new conectar();
$conexion = $c->conexion();
$sql = "SELECT art.name_prod,
					art.description,
					art.amount,
					art.price,
					img.route,
					cat.cat_name,
					art.id_prod
		  from articles as art 
		  inner join images as img
		  on art.id_img=img.id_img
		  inner join categories as cat
		  on art.id_cat=cat.id_cat";
$result = mysqli_query($conexion, $sql);

/*
var_dump("<b>SQL=</b> " . $sql);
//var_dump("RESULTADO=  " . $result);
var_dump("<b>RESULTADO:</b> ");
var_dump($result);
*/
?>

<table class="table table-hover table-condensed table-bordered" style="text-align: center; ">
	<caption><label>Articulos</label></caption>
	<tr style="font-weight: bold;">
		<td>Nombre</td>
		<td>Descripcion</td>
		<td>Cantidad</td>
		<td>Precio</td>
		<td>Imagen</td>
		<td>Categoria</td>
		<td>Editar</td>
		<td>Eliminar</td>
	</tr>

	<?php while ($ver = mysqli_fetch_row($result)): ?>

		<tr>
			<td><?php echo $ver[0]; ?></td>
			<td><?php echo $ver[1]; ?></td>
			<td><?php echo $ver[2]; ?></td>
			<td><?php echo "$./" . $ver[3]; ?></td>
			<td>
				<?php
				$imgVer = explode("/", $ver[4]);
				$imgRuta = $imgVer[1] ."/". $imgVer[2] . "/" . $imgVer[3];

				?>
				<img width="70" height="70" src="<?php echo $imgRuta ?>">
			</td>
			<td><?php echo $ver[5]; ?></td>
			<td>
				<span data-bs-toggle="modal" data-bs-target="#abremodalUpdateArticulo" class="btn btn-warning btn-xs" onclick="agg_data_articles('<?php echo $ver[6]; ?>')">
					<span class="glyphicon glyphicon-pencil">Act</span>
				</span>
			</td>
			<td>
				<span class="btn btn-danger btn-xs" onclick="delete_articles('<?php echo $ver[6]; ?>')">
					<span class="glyphicon glyphicon-remove">Elim</span>
				</span>
			</td>
		</tr>
	<?php endwhile; ?>
</table>