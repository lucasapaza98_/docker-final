 <nav class="navbar navbar-expand-lg" style="background-color: #e3f2fd;">
    <div class="container-fluid">

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" style="font-weight: bold;" aria-current="page" href="inicio.php">Inicio</a>
          </li>

          <li class="nav-item">
            <a class="nav-link active" style="font-weight: bold;" href="categories.php">Categorias</a>
          </li>

          <li class="nav-item">
            <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="articles.php">Articulos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="client.php">Clientes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" style="font-weight: bold;" aria-disabled="true" href="sales.php">Vender Articulos</a>
          </li>



        </ul>

        <a href="#" style="color:green; font-weight: bold; ">Email: <?php echo $_SESSION['email']; ?> <span class="caret"></span></a>

        <?php
        if ($_SESSION['email'] == "lucasezequielapaza15@gmail.com"):
        ?>
          <a href="users.php"> <button class="btn btn-outline-success" type="submit">Usuarios</button></a>

        <?php
        endif;
        ?>


        <a href="../process/salir.php"> <button class="btn btn-outline-success" type="submit">Salir</button></a>
      </div>
    </div>
  </nav>