<?php
require_once "../../class/connection.php";
$c = new conectar();
$conexion = $c->conexion();

//$sql = "SELECT id_user,name,last_name,email FROM users";

$sql  = "SELECT u.id_user,u.name,u.last_name,r.name_rol,u.email FROM users AS u INNER JOIN roles AS r ON u.id_rol = r.id_rol";


$result = mysqli_query($conexion, $sql);

var_dump("<b>RESULTADO DE LA CONSULTA</b> <br>");
var_dump( $result);
var_dump("<b>SQL</b> <br>");
var_dump($sql);

?>

<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
	<caption><label>Usuarios </label></caption>
	<tr>
		<td>Nombre</td>
		<td>Apellido</td>
		<td>Rol</td>
		<td>Email</td>
		
		<td>Editar</td>
		<td>Eliminar</td>
	</tr>

	<?php while ($ver = mysqli_fetch_row($result)): 
		?>



		<tr>
			<td><?php echo $ver[1]; ?></td>
			<td><?php echo $ver[2]; ?></td>
			<td><?php echo $ver[3]; ?></td>
			<td><?php echo $ver[4]; ?></td>

			<td>
				<span class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#updateUsersModal" onclick="aggDatoUsers('<?php echo $ver[0]; ?>')">
					<span class="glyphicon glyphicon-pencil">Act</span>
				</span>
			</td>
			<td>
				<span class="btn btn-danger btn-sm" onclick="delete_user('<?php echo $ver[0]; ?>')">
					<span class="glyphicon glyphicon-remove">Elim</span>
				</span>
			</td>
		</tr>

	<?php endwhile; ?>


</table>