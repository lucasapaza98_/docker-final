<?php
session_start();
if (isset($_SESSION['email'])) {

?>


    <!DOCTYPE html>
    <html>

    <head>
        <title>Sistemas de Ventas - Articles</title>
        <?php require_once "main.php"; ?>
        <?php require_once "../class/connection.php";
        $c = new conectar();
        $conexion = $c->conexion();
        $sql = "SELECT id_cat,cat_name
		from categories";
        $result = mysqli_query($conexion, $sql);
        ?>
    </head>

    <body>
        <div class="container">
            <h1>Articulos</h1>
            <div class="row">
                <div class="col-sm-4">
                    <form id="frmArticulos" enctype="multipart/form-data">
                        <label>Categoria</label>
                        <select class="form-control input-sm" id="categoriaSelect" name="categoriaSelect">
                            <option value="A">Selecciona Categoria</option>
                            <?php while ($ver = mysqli_fetch_row($result)): ?>
                                <option value="<?php echo $ver[0] ?>"><?php echo $ver[1] ?></option>
                            <?php endwhile; ?>
                        </select>
                        <label>Nombre</label>
                        <input type="text" class="form-control input-sm" id="nameArt" name="nameArt">
                        <label>Descripcion</label>
                        <input type="text" class="form-control input-sm" id="descripcion" name="descripcion">
                        <label>Cantidad</label>
                        <input type="number" class="form-control input-sm" id="cant" name="cant">
                        <label>Precio</label>
                        <input type="number" class="form-control input-sm" id="precio" name="precio">
                        <label>Imagen</label>
                        <input type="file" id="imagen" name="imagen">
                        <p></p>
                        <span id="btnAgregaArticulo" class="btn btn-primary">Agregar</span>
                    </form>
                </div>
                <div class="col-sm-8">
                    <div id="tablaArticulosLoad"></div>
                </div>
            </div>
        </div>

        <!-- Button trigger modal -->

        <!-- Modal -->
        <div class="modal fade" id="abremodalUpdateArticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Actualiza Articulo</h4>
                    </div>
                    <div class="modal-body">
                        <form id="frmArticulosU" enctype="multipart/form-data">
                            <input type="text" id="id_articles" name="id_articles">
                            <label>Categoria</label>
                            <select class="form-control input-sm" id="categoriaSelectU" name="categoriaSelectU">
                                <option value="A">Selecciona Categoria</option>
                                <?php
                                $sql = "SELECT id_cat,cat_name
								from categories";
                                $result = mysqli_query($conexion, $sql);
                                ?>
                                <?php while ($ver = mysqli_fetch_row($result)): ?>
                                    <option value="<?php echo $ver[0] ?>"><?php echo $ver[1]; ?></option>
                                <?php endwhile; ?>
                            </select>
                            <label>Nombre</label>
                            <input type="text" class="form-control input-sm" id="nameArtU" name="nameArtU">
                            <label>Descripcion</label>
                            <input type="text" class="form-control input-sm" id="descripcionU" name="descripcionU">
                            <label>Cantidad</label>
                            <input type="number" class="form-control input-sm" id="cantU" name="cantU">
                            <label>Precio</label>
                            <input type="number" class="form-control input-sm" id="precioU" name="precioU">

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="btnActualizaarticulo" type="button" class="btn btn-warning" data-dismiss="modal">Actualizar</button>

                    </div>
                </div>
            </div>
        </div>

    </body>

    </html>

    <script type="text/javascript">
        function agg_data_articles(idarticulo) {
            $.ajax({
                type: "POST",
                data: "id_art=" + idarticulo,
                url: "../process/articles/obten_data_articles.php",
                success: function(r) {

                    dato = jQuery.parseJSON(r);
                    $('#id_articles').val(dato['id_prod']);
                    $('#categoriaSelectU').val(dato['id_cat']);
                    $('#nameArtU').val(dato['name_prod']);
                    $('#descripcionU').val(dato['description']);
                    $('#cantU').val(dato['amount']);
                    $('#precioU').val(dato['price']);

                    console.log(dato);

                }
            });
        }

        function delete_articles(idArticulo) {
            alertify.confirm('¿Desea eliminar este articulo?', function() {
                $.ajax({
                    type: "POST",
                    data: "id_art=" + idArticulo,
                    url: "../process/articles/delete_articles.php",
                    success: function(r) {
                        if (r == 1) {
                            $('#tablaArticulosLoad').load("articles/table_articles.php");
                            alertify.success("Eliminado con exito!!");
                        } else {
                            alertify.error("No se pudo eliminar :(");
                            console.log(r);
                        }
                    }
                });
            }, function() {
                alertify.error('Cancelo !')
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#btnActualizaarticulo').click(function() {

                datos = $('#frmArticulosU').serialize();
                $.ajax({
                    type: "POST",
                    data: datos,
                    url: "../process/articles/update_articles.php",
                    success: function(r) {
                        if (r == 1) {
                            $('#tablaArticulosLoad').load("articles/table_articles.php");
                            alertify.success("Actualizado con exito :D");
                            console.log(r);
                        } else {
                            alertify.error("Error al actualizar :(");
                            console.log(r);
                        }
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#tablaArticulosLoad').load("articles/table_articles.php");
            $('#btnAgregaArticulo').click(function() {

                vacios = validarFormVoid('frmArticulos');

                if (vacios > 0) {
                    alertify.alert("Debes llenar todos los campos!!");

                    // console.log("VACIOS: " + vacios);

                    return false;
                }


                var formData = new FormData(document.getElementById("frmArticulos"));

                $.ajax({
                    url: "../process/articles/inserta_articles.php",
                    type: "POST",
                    dataType: "html",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,

                    success: function(r) {
                        if (r == 2) {
                            alertify.alert("Articulos duplicado ingrese otro");
                            console.log(r);
                            
                            return false;

                        } else if (r == 1) {
                            $('#frmArticulos')[0].reset();
                            $('#tablaArticulosLoad').load("articles/table_articles.php");
                            alertify.success("Agregado con exito :D");
                            console.log(r);
                        } else {
                            alertify.error("Fallo al subir el archivo :(");

                            console.log(r);
                        }
                    }
                });

            });
        });
    </script>

<?php
} else {
    header("location:../index.php");
}
?>