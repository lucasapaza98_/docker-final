
<?php

require_once "../../class/connection.php";
$c= new conectar();
$conexion=$c->conexion();

$sql="SELECT id_cat,cat_name FROM categories";

$result = mysqli_query($conexion,$sql);



?>

<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
	<caption><label>Tabla de Categorias</label></caption>
	<tr style="font-weight: bold;">
		<td>Categoria</td>
		<td>Editar</td>
		<td>Eliminar</td>
	</tr>

	<?php
	while ($mostrar=mysqli_fetch_row($result)):

	?>

	

	<tr>
		<td> <?php echo $mostrar[1];?></td>
		<td>
			<span class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#update_cat" onclick="agg_datos('<?php echo $mostrar[0] ?>','<?php echo $mostrar[1] ?>')">	
			<i class="bi bi-pencil"></i>Act</span>
			</span>	
		</td>	
		<td>
			<span class="btn btn-danger btn-sm" onclick="delete_categories('<?php echo $mostrar[0] ?>')">
				<span class="glyphicon glyphicon-remove"><i class="fas fa-remove"></i>Elim</span>
			</span>
		</td>
	</tr>

	<?php endwhile;?>


</table>