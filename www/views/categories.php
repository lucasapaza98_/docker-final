<?php
session_start();
if (isset($_SESSION['email'])) {

	var_dump($_SESSION['id_user']);
	var_dump($_SESSION['email']);



?>


	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistemas Ventas - Categorias</title>

		<?php require_once "main.php"; ?>
	</head>

	<body>
		<div class="container">
			<h1>Categorias</h1>
			<div class="row">
				<div class="col-sm-4">
					<form id="frmCategorias">
						<label>Categoria</label>
						<input type="text" class="form-control input-sm" name="categoria" id="categoria">
						<p></p>
						<span class="btn btn-primary" id="btnAgregaCategoria">Agregar</span>
					</form>
				</div>
				<div class="col-sm-6">
					<div id="tablaCategoriaLoad"></div>
				</div>
			</div>
		</div>



		<!-- Modal -->
		<div class="modal fade" id="update_cat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Actualizar categorias</h4>
					</div>
					<div class="modal-body">
						<form id="frmCategoriasU">
							<input type="text" name="idCategories" id="idCategories" hidden>
							<label>Categorias</label>
							<input type="text" name="categoriesU" id="categoriesU" class="form-control input-sm">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btn_update_cat" class="btn btn-warning" data-dismiss="modal">Actualizar</button>
					</div>
				</div>
			</div>
		</div>





	</body>

	</html>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#tablaCategoriaLoad').load("categories/table_categories.php");

			$('#btnAgregaCategoria').click(function() {

				vacios = validarFormVoid('frmCategorias');

				if (vacios > 0) {
					alertify.alert("Debes llenar todos los campos!!");
					return false;
				}

				datos = $('#frmCategorias').serialize();
				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/categories/agg_categories.php",
					success: function(r) {

						if (r == 2) {
							alertify.alert("Categoria duplicada ingrese otro");


						} else if (r == 1) {
							//esta linea nos permite limpiar el formulario al insertar un registro
							$('#frmCategorias')[0].reset();
							$('#tablaCategoriaLoad').load("categories/table_categories.php");
							alertify.success("Categoria agregada con exito");

							console.log(r);

						} else {

							console.log(r);

							alertify.error("No se pudo agregar categoria");
						}
					}
				});
			});
		});
	</script>

	<script>
		$(document).ready(function() {
			$('#btn_update_cat').click(function() {
				datos = $('#frmCategoriasU').serialize();
				$.ajax({
					type: "POST",
					data: datos,
					url: "../process/categories/update_categories.php",
					success: function(r) {

						if (r == 1) {
							$('#tablaCategoriaLoad').load("categories/table_categories.php");
							alertify.success("Actualizado con exito");

						} else {
							alertify.error("No se pudo actualizar");
						}

					}
				});
			});

		});
	</script>

	<script type="text/javascript">
		function agg_datos(id_categories, categories) {
			$('#idCategories').val(id_categories);
			$('#categoriesU').val(categories);


			//console.log(id_categories, categories);
			//console.log(id_categories);
			//console.log(categories);


		}

		function delete_categories(id_categories) {
			alertify.confirm('Desea eliminar la categoria', function() {
				$.ajax({
					type: "POST",
					data: "id_categories=" + id_categories,
					url: "../process/categories/delete_categories.php",
					success: function(r) {
						if (r == 1) {
							$('#tablaCategoriaLoad').load("categories/table_categories.php");
							alertify.success("Eliminado con exito");
							console.log(r);

						} else {
							alertify.error("Error al eliminar");
							console.log(r);

						}

					}
				});
			}, function() {
				alertify.error('Cancelado')
			});

		}
	</script>
<?php
} else {
	header("location:../index.php");
}

?>