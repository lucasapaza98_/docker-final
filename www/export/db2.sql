CREATE DATABASE Futbol;

USE Futbol;

CREATE TABLE Equipos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    ciudad VARCHAR(100) NOT NULL,
    estadio VARCHAR(100) NOT NULL
);

CREATE TABLE Jugadores (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    edad INT NOT NULL,
    posicion VARCHAR(50) NOT NULL,
    equipo_id INT,
    FOREIGN KEY (equipo_id) REFERENCES Equipos(id)
);

CREATE TABLE Partidos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    equipo_local_id INT,
    equipo_visitante_id INT,
    fecha DATE NOT NULL,
    goles_local INT,
    goles_visitante INT,
    FOREIGN KEY (equipo_local_id) REFERENCES Equipos(id),
    FOREIGN KEY (equipo_visitante_id) REFERENCES Equipos(id)
);


